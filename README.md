# Bitbucket Pipelines Pipe: Google App Engine Deploy

Pipe to deploy an application to [Google App Engine][gae-deploy].

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/google-app-engine-deploy:0.6.1
  variables:
    KEY_FILE: '<string>'
    PROJECT: '<string>'
    # DEPLOYABLES: '<string>' # Optional
    # VERSION: '<string>' # Optional.
    # BUCKET: '<string>' # Optional.
    # IMAGE: '<string>' # Optional.
    # PROMOTE: '<boolean>' # Optional
    # STOP_PREVIOUS_VERSION: '<boolean>' # Optional.
    # EXTRA_ARGS: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
    # CLOUD_BUILD_TIMEOUT: '<integer>' # Optional
```

## Variables

| Variable                   | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| KEY_FILE (*)                  |  base64 encoded Key file for a [Google service account](https://cloud.google.com/iam/docs/creating-managing-service-account-keys). |
| PROJECT (*)                   |  The Project ID of the project that owns the app to deploy. |
| DEPLOYABLES                   |  List of white space separated yaml files to be passed to gcloud. Default: `app.yaml`. |
| VERSION                       |  The version of the app to be created/replaced. |
| BUCKET                        |  The google cloud storage bucket to store the files associated with the deployment. |
| IMAGE                         |  Deploy with a specific GCR docker image. |
| PROMOTE                       |  If true the deployed version is receives all traffic, false to not receive traffic. |
| STOP_PREVIOUS_VERSION         |  If true the previous version receiving traffic is stopped, false to not stop the previous version. |
| EXTRA_ARGS                    |  Extra arguments to be passed to the CLI (see [Google app deploy docs][gae-deploy] for more details). |
| DEBUG                         |  Turn on extra debug information. Default `false`. |
| CLOUD_BUILD_TIMEOUT           |  Timeout, in seconds, to wait for Docker builds to complete during deployments. |

_(*) = required variable._


## Details

With the Google App Engine Deploy pipe you can deploy your application to [Google App Engine][gae-deploy].
All [gcloud components][gcloud components] are installed.
For Java applications pipe includes `java -version openjdk version "1.8.0_212"`.
Supports deploying Java apps, Spring Boot based Java apps built with Maven and Gradle files app.yaml or WEB-INF/appengine-web.xml.

More info about parameters and values can be found in the [Google app deploy docs][gae-deploy].


## Prerequisites

* An IAM user is configured with sufficient permissions to perform a deployment of your application using gcloud.
* You have [enabled APIs and services](https://cloud.google.com/service-usage/docs/enable-disable) needed for your application.


## Examples

### Basic example:

```yaml
script:
  - pipe: atlassian/google-app-engine-deploy:0.6.1
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'my-project'
```

### Advanced example:

Deploy multiple deployables.
    
```yaml
script:
  - pipe: atlassian/google-app-engine-deploy:0.6.1
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'my-project'
      DEPLOYABLES: 'app-1.yaml app-2.yaml'
      VERSION: 'alpha'
      BUCKET: 'gs://my-bucket'
      IMAGE: 'gcr.io/my/image'
      PROMOTE: 'true'
      STOP_PREVIOUS_VERSION: 'true'
      EXTRA_ARGS: '--logging=debug'
```

Deploy a [Python app][Python app] to gcloud.
```yaml
script:
  - pipe: atlassian/google-app-engine-deploy:0.6.1
    variables:
      KEY_FILE: $KEY_FILE_PYTHON_APP
      PROJECT: 'pipes-example-python3-app'
      DEPLOYABLES: 'hello_world/app.yaml'
      VERSION: '${BITBUCKET_BUILD_NUMBER}'
      PROMOTE: 'true'
      STOP_PREVIOUS_VERSION: 'true'
      EXTRA_ARGS: '--verbosity=debug --quiet'
```

Deploy a [Spring Boot based Java app][Spring Boot based Java app] build with Maven `app.yaml` to gcloud.
```yaml
script:
  - pipe: atlassian/google-app-engine-deploy:0.6.1
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'pipes-java-maven-spring-boot'
      DEPLOYABLES: 'target/appengine-staging/app.yaml'
      VERSION: '${BITBUCKET_BUILD_NUMBER}'
      EXTRA_ARGS: '--verbosity=debug'
```

Deploy a [Spring Boot based Java app][Spring Boot based Java app] build with Gradle `app.yaml` to gcloud.
```yaml
script:
  - pipe: atlassian/google-app-engine-deploy:0.6.1
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'pipes-java-gradle-spring-boot'
      DEPLOYABLES: 'build/staged-app/app.yaml'
      # DEPLOYABLES: 'src/main/WEB-INF/appengine-web.xml' # or you can provide web.xml
      VERSION: '${BITBUCKET_BUILD_NUMBER}'
      EXTRA_ARGS: '--verbosity=debug'
```

Deploy a [Spring Boot based Java app][Spring Boot based Java app] build with Gradle `WEB-INF/appengine-web.xml` to gcloud.
```yaml
script:
  - pipe: atlassian/google-app-engine-deploy:0.6.1
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'pipes-java-gradle-spring-boot'
      DEPLOYABLES: 'src/main/WEB-INF/appengine-web.xml'
      VERSION: '${BITBUCKET_BUILD_NUMBER}'
      EXTRA_ARGS: '--verbosity=debug'
```

Deploy a [standard Java app][standard Java app] build with Maven `app.yaml` to gcloud.
```yaml
script:
  - pipe: atlassian/google-app-engine-deploy:0.6.1
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'pipes-example-java-app-maven'
      DEPLOYABLES: 'target/appengine-staging/app.yaml'
      VERSION: '${BITBUCKET_BUILD_NUMBER}'
      PROMOTE: 'true'
      STOP_PREVIOUS_VERSION: 'true'
      EXTRA_ARGS: '--verbosity=debug'
```

Deploy a [standard Java app][standard Java app] build with Maven `WEB-INF/appengine-web.xml` to gcloud.
```yaml
script:
  - pipe: atlassian/google-app-engine-deploy:0.6.1
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'pipes-example-java-app-maven'
      DEPLOYABLES: 'target/appengine-staging/WEB-INF/appengine-web.xml'
      VERSION: '${BITBUCKET_BUILD_NUMBER}'
      PROMOTE: 'true'
      STOP_PREVIOUS_VERSION: 'true'
      EXTRA_ARGS: '--verbosity=debug'
```

Deploy a [standard Java app][standard Java app] build with Gradle `WEB-INF/appengine-web.xml` to gcloud.
```yaml
script:
  - pipe: atlassian/google-app-engine-deploy:0.6.1
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'pipes-example-java-app-gradle'
      DEPLOYABLES: 'build/exploded-helloworld/WEB-INF/appengine-web.xml'
      VERSION: '${BITBUCKET_BUILD_NUMBER}'
      PROMOTE: 'true'
      STOP_PREVIOUS_VERSION: 'true'
      EXTRA_ARGS: '--verbosity=debug'
```

Deploy a [standard Java app][standard Java app] build with Gradle `app.yaml` to gcloud.
```yaml
script:
  - pipe: atlassian/google-app-engine-deploy:0.6.1
    variables:
      KEY_FILE: $KEY_FILE
      PROJECT: 'pipes-example-java-app-gradle'
      DEPLOYABLES: 'build/staged-app/app.yaml'
      VERSION: '${BITBUCKET_BUILD_NUMBER}'
      PROMOTE: 'true'
      STOP_PREVIOUS_VERSION: 'true'
      EXTRA_ARGS: '--verbosity=debug'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,google,gae
[gae-deploy]: https://cloud.google.com/sdk/gcloud/reference/app/deploy
[Spring Boot based Java app]: https://github.com/GoogleCloudPlatform/getting-started-java/tree/master/helloworld-springboot
[standard Java app]: https://github.com/GoogleCloudPlatform/getting-started-java/tree/master/appengine-standard-java8/helloworld
[Python app]: https://github.com/GoogleCloudPlatform/python-docs-samples/tree/master/appengine/standard_python37/hello_world
[gcloud components]: https://cloud.google.com/sdk/docs/components#additional_components
