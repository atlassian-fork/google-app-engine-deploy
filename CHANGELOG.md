# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.6.1

- patch: Update the Readme with a new Atlassian Community link.

## 0.6.0

- minor: Bump google/cloud-sdk version to 273.0.0

## 0.5.1

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.5.0

- minor: Added a new parameter to set the cloud build timeout

## 0.4.1

- patch: Internal maintenance: Update Readme with Java examples.

## 0.4.0

- minor: Internal maintenance: use large docker image google-cloud-sdk
- patch: Add parameter --quiet to main deploy command and prerequisites section to Readme

## 0.3.1

- patch: Switched to google/cloud-sdk:242.0.0-alpine image

## 0.3.0

- minor: Allow passing multiple deployment configs (deployables)

## 0.2.2

- patch: Updated contributing guidelines

## 0.2.1

- patch: Standardising README and pipes.yml.

## 0.2.0

- minor: Add support for the DEBUG variable.
- minor: Switch naming conventions from task to pipes.

## 0.1.3

- patch: Use quotes for all pipes examples in README.md.

## 0.1.2

- patch: Remove details.

## 0.1.1

- patch: Restructure README.md to match user flow.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines GCP app engine deploy pipe.

